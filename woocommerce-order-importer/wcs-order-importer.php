<?php
/**
 * Plugin Name: WooCommerce Order CSV Importer
 * Description: Import or export order in your WooCommerce store via CSV.
 * Version: 1.0
 * Author: Wisdmlabs
 * Author URI: http://wisdmlabs.com
 * License: GPLv3
 *
 *
 * @package		WooCommerce Order Importer
 * @author		WisdmLabs.
 * @since		1.0
 */
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

if ( ! function_exists( 'woothemes_queue_update' ) || ! function_exists( 'is_woocommerce_active' ) ) {
	require_once( 'woo-includes/woo-functions.php' );
}

require_once( 'includes/wcsi-functions.php' );

class WDM_Order_Importer {

	public static $wcs_importer;

	public static $wcs_exporter;

	public static $version = '1.0.0';

	protected static $plugin_file = __FILE__;

	/**
	 * Initialise filters for the Subscriptions CSV Importer
	 *
	 * @since 1.0
	 */
	public static function init() {
		add_filter( 'plugins_loaded', __CLASS__ . '::setup_importer' );
		add_filter( 'plugin_action_links_' . plugin_basename( __FILE__ ), __CLASS__ . '::action_links' );

		//spl_autoload_register( __CLASS__ . '::autoload' );
	}

	/**
	 * Create an instance of the importer on admin pages and check for WooCommerce Subscriptions dependency.
	 *
	 * @since 1.0
	 */
	public static function setup_importer() {

		if ( is_admin() ) {
			if ( class_exists( 'WooCommerce' ) ) {
				require_once( self::plugin_dir() . '/includes/class-wcs-import-admin.php' );
				require_once( self::plugin_dir() . '/includes/class-wcs-importer.php' );
				//self::$wcs_exporter = new WCS_Export_Admin();
				self::$wcs_importer = new WCS_Order_Import_Admin();
			} else {
				add_action( 'admin_notices', __CLASS__ . '::plugin_dependency_notice' );
			}
		}
	}

	/**
	 * Include Docs & Settings links on the Plugins administration screen
	 *
	 * @since 1.0
	 * @param mixed $links
	 */
	public static function action_links( $links ) {

		$plugin_links = array(
			'<a href="' . esc_url( admin_url( 'admin.php?page=import_order' ) ) . '">' . esc_html__( 'Import', 'wcs-import-export' ) . '</a>',
		);

		return array_merge( $plugin_links, $links );
	}

	/**
	 * Display error message according to the missing dependency
	 *
	 * Will only show error for missing WC if Subscriptions is missing as well,
	 * this is to avoid duplicating messages printed by Subscriptions.
	 *
	 * @since 1.0
	 */
	public static function plugin_dependency_notice() {

		if ( ! class_exists( 'WooCommerce' )) :?>
			<div id="message" class="error">
				<p><?php printf( esc_html__( '%1$sWooCommerce Order Importer is inactive.%2$s The %3$sWooCommerce plugin%4$s must be active for WooCommerce Order Importer to work. Please %5$sinstall & activate%6$s WooCommerce.', 'wcs-import-export' ), '<strong>', '</strong>', '<a href="https://wordpress.org/plugins/woocommerce/">', '</a>', '<a href="' . esc_url( admin_url( 'plugins.php' ) ) . '">', '</a>' ); ?></p>
			</div>
		<?php endif;
	}

	/**
	 * Get the plugin's URL path for loading assets
	 *
	 * @since 2.0
	 * @return string
	 */
	public static function plugin_url() {
		return plugin_dir_url( self::$plugin_file );
	}

	/**
	 * Get the plugin's path for loading files
	 *
	 * @since 2.0
	 * @return string
	 */
	public static function plugin_dir() {
		return plugin_dir_path( self::$plugin_file );
	}

	/**
	 * Get the plugin's path for loading files
	 *
	 * @since 2.0
	 * @return string
	 */
	public static function autoload( $class ) {
		$class = strtolower( $class );
		$file  = 'class-' . str_replace( '_', '-', $class ) . '.php';

		if ( 0 === strpos( $class, 'wcs_import' ) ) {
			error_log('file');
			error_log(print_r($file, 1));
			require_once( self::plugin_dir() . '/includes/' . $file );
		}
	}
}

WDM_Order_Importer::init();
